class WrackMoleGame {
    constructor(colCount, rowCount) {
        this.table = null;
        this.allTd = null;
        this.playerScore = 0;
        this.computerScore = 0;
        this.activeCell = null;
        this.colCount = colCount;
        this.rowCount = rowCount;
        this.level = null;
        this.result = null;
        this.winner = null;
        this.selectLevel = null;
        this.dificultLevels = {
            hard: 500,
            medium: 1000,
            easy: 1500,
        };
    }

    replaceClass(elem, oldClassName, newClassName) {
        if (elem && elem.classList.contains(oldClassName)) {
            elem.classList.remove(oldClassName);
            elem.classList.add(newClassName);
            return true;
        }
        return false;
    }

    createEmptyElements(tagName, tagNumber, tagClass) {
        const arr = Array(tagNumber).fill(0).map((item) => {
            const elem = document.createElement(tagName);
            elem.className = tagClass || "";
            return elem;
        });
        return arr;
    }

    randomNumber(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    selectRandomElem(arr) {
        const index = this.randomNumber(0, arr.length - 1);
        [this.activeCell] = arr.splice(index, 1);
        this.activeCell.classList.add("active");
    }

    startGame() {
        this.playerScore = 0;
        this.computerScore = 0;
        this.createTable();
            let gameIndex = setInterval(() => {
                if (this.replaceClass(this.activeCell, "active", "fail")) {
                    this.computerScore++;
                }
                if (this.computerScore === 50 || this.playerScore === 50) {
                    clearInterval(gameIndex);
                    this.result = document.createElement('p');
                    this.result.textContent = `playerScore - ${this.playerScore}. computerScore - ${this.computerScore}`;
                    this.table.after(this.result);
                    this.winner = document.createElement('p');
                    if (this.computerScore === 50) {
                        this.winner.textContent = "Победил Компьютер!!!";
                    } else {
                        this.winner.textContent = "Победил Игрок!!!";
                    }
                    this.result.after(this.winner);
                }
                else {
                this.selectRandomElem(this.allTd);
                }
            }, this.level);
        }
        createTable(){
        if (this.table){
            this.table.remove();
        }
        if (this.result){
            this.result.remove();
        }
            if (this.winner){
                this.winner.remove();
            }
            this.level = this.dificultLevels[this.selectLevel.value];

            this.table = document.createElement("table");
            this.table.addEventListener("click", ({target}) => {
                if (this.replaceClass(target, "active", "success")) {
                    this.playerScore++;
                }
            });
            this.allTd = [];
            for (let i = 0; i < this.rowCount; i++) {
                const tr = document.createElement("tr");
                const tdList = this.createEmptyElements("td", this.colCount);
                this.allTd = [...this.allTd, ...tdList];
                tr.append(...tdList);
                this.table.append(tr);
            }
            this.gameContainer.prepend(this.table);
         }
    render(elem) {
        this.gameContainer = elem;
        this.selectLevel = document.createElement('select');
        const option1 = document.createElement('option');
        option1.value = 'hard';
        option1.textContent = 'Сложный';
        const option2 = document.createElement('option');
        option2.value = 'medium';
        option2.textContent = 'Средний';
        const option3 = document.createElement('option');
        option3.value = 'easy';
        option3.textContent = 'Легкий';
        this.selectLevel.append(option1, option2, option3);
        this.createTable();
        this.gameContainer.append(this.selectLevel);
        const buttonStart = document.createElement('button');
        buttonStart.textContent = 'start';
        buttonStart.addEventListener('click', this.startGame.bind(this));
        this.gameContainer.append(buttonStart);
    }
}

const wrackMoleGame = new WrackMoleGame(10, 10);
const gameContainer = document.querySelector("#elem");
wrackMoleGame.render(gameContainer);